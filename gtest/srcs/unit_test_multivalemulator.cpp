/// Copyright (c) 2016, Jean-Baptiste Laurent
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
///
/// 1. Redistributions of source code must retain the above copyright notice, this
///    list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation
///    and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
/// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
/// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
/// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
/// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
/// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
/// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
/// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
/// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
///
/// The views and conclusions contained in the software and documentation are those
/// of the authors and should not be interpreted as representing official policies,
/// either expressed or implied, of the Barghest Project.
///

#include "unit_test_valemulator.hh"

template <typename T>
ValEmulator_test<T>::ValEmulator_test()
{
}

template <typename T>
ValEmulator_test<T>::~ValEmulator_test()
{
}

template <typename T>
auto        ValEmulator_test<T>::testing_operator() -> void
{
  T        a;
  T        b;
  unsigned int    i;

  for (i = 0 ; i < 1000 ; i++)
  {
// TODO: resoudre le soucis dans bitFieldArithmetic sur l'exception C++ lorsque i >= 1000
    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu += (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a + b));

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu -= (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a - b));

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu *= (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a * b));

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu /= (b = ((b = _gen_rand_nb()) ? b : 1));

    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a / b));

    // this->_ValEmu %= (b = _gen_rand_nb());

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu <<= (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a << b));

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu >>= (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a >> b));

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu |= (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a | b));

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu &= (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a & b));

    this->_ValEmu.set((a = _gen_rand_nb()));
    this->_ValEmu ^= (b = _gen_rand_nb());
    EXPECT_EQ(_ValEmu.getMin(), _ValEmu.getMax());
    EXPECT_EQ(_ValEmu.getMax(), (T)(a ^ b));
  }
}

template class ValEmulator_test <uint8_t>;
template class ValEmulator_test <uint16_t>;
template class ValEmulator_test <uint32_t>;

template class ValEmulator_test <int8_t>;
template class ValEmulator_test <int16_t>;
template class ValEmulator_test <int32_t>;

#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
template class ValEmulator_test <uint64_t>;
template class ValEmulator_test <int64_t>;
#endif /* !64b */
