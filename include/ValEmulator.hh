/// Copyright (c) 2016, Jean-Baptiste Laurent
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
///
/// 1. Redistributions of source code must retain the above copyright notice, this
///    list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation
///    and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
/// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
/// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
/// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
/// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
/// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
/// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
/// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
/// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
///
/// The views and conclusions contained in the software and documentation are those
/// of the authors and should not be interpreted as representing official policies,
/// either expressed or implied, of the Barghest Project.
///
/// @brief
/// This class was supposed to give a generic way to handle value emulator like:
/// * bitField -> Aimed to support quadri value binary arithmetic
/// * boostInterval -> Aimed to support standard arithmetic + branching filtering
/// Ex:
/// int a = randomValue;
///
/// if (a > 5)
///   // A is always greater than 5 ([6, +inf]]
/// else
///   // a is lower or equal than 5 ([-inf, 5])
///
/// However, for simplicity and speed we use an other class who agregate
/// directly those ValEmulator in order to handler the data without
/// any 'long' convertion and error prone code. (which is a also lot faster)
///

#ifndef VAL_EMULATOR_HH_
# define VAL_EMULATOR_HH_

# include <iosfwd>            // foward definition (std::ostream)
# include <cstdlib>            // size_t
# include <bitField/bitfield_t.hpp>        // bitfield_t
# include <bitField/BitFieldArithmetic/BitFieldArithmetic.hh>    // BitFieldArithmetic<T>

template <typename T = size_t>
class ValEmulator
{
private:
  bitField::BitFieldArithmetic::t_bitFieldEflags        _eflags;
  bitField::bitfield_t<T>                               _bitFieldMem;
  bitField::BitFieldArithmetic::BitFieldArithmetic<T>   _bitFieldArith;
private:
  int            _boostIntervalHere;
public:
  enum e_inc
  {
    NO = 0,     /* 0 */
    YES,        /* 1 */
    UNKNOWN,    /* 2 */
    UNDEFINED   /* 3 */
  };

public:
  /*!
  ** Default constructor, reset all values.
  */
  ValEmulator(void);
  ValEmulator(T const &);
  ~ValEmulator(void);

  /*!
  ** Change the values emulated but do not change the eflags.
  */
  auto    set(T const &) -> void;

  /*!
  ** Reset all type to the 'undefined'(U) state.
  */
  auto    reset(void) -> void;

  // auto    empty(void) const -> bool;
  // auto    whole(void) const -> bool;

  auto    getMin(void) const -> T;
  auto    getMax(void) const -> T;

  auto    getEflags(void) const -> bitField::BitFieldArithmetic::t_bitFieldEflags const &;
  auto    clrEflags(void) -> void;
  auto    resetEflags(void) -> void;

  // auto    lower(void) const -> T;
  // auto    upper(void) const -> T;
  // auto    width(void) const -> T;
  // auto    overlap(ValEmulator<T> const &) -> e_inc;
  // auto    subset(ValEmulator<T> const &) -> e_inc;

  // auto    addVal(T const &) -> void;
  // auto    delVal(T const &) -> void;

  // auto    contain(T const &) const -> bool;
  // auto    exclude(T const &) const -> bool;

  // auto    operator=(ValEmulator<T> const &) -> ValEmulator<T> &;
  // auto    operator==(ValEmulator<T> const &) const -> e_inc;

  auto    operator+=(ValEmulator<T> const &) -> ValEmulator<T> &;
  // auto    operator++(void) -> ValEmulator<T> &;
  auto    operator-=(ValEmulator<T> const &) -> ValEmulator<T> &;
  // auto    operator--(void) -> ValEmulator<T> &;
  auto    operator*=(ValEmulator<T> const &) -> ValEmulator<T> &;
  auto    operator/=(ValEmulator<T> const &) -> ValEmulator<T> &;
  // auto    operator%=(ValEmulator<T> const &) -> ValEmulator<T> &;
  auto    operator<<=(ValEmulator<T> const &) -> ValEmulator<T> &;
  auto    operator>>=(ValEmulator<T> const &) -> ValEmulator<T> &;
  auto    operator|=(ValEmulator<T> const &) -> ValEmulator<T> &;
  auto    operator&=(ValEmulator<T> const &) -> ValEmulator<T> &;
  auto    operator^=(ValEmulator<T> const &) -> ValEmulator<T> &;
};

template <typename T>
auto    operator<<(std::ostream &os, ValEmulator<T> const &) -> std::ostream &;

#endif /* !VAL_EMULATOR_HH_ */
