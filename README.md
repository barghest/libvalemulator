# ValEmulator

## What is ValEmulator

ValEmulator is a class used to emulate a variable in memory from a regular
type (Ex: char, short, int, etc), for a usage in a static context. Meaning
it can perform operations on a variable with multiple values.

Internaly is uses multiple value representation (such as interval, and bitfield)
to provide a precise idea of what the values the variable can hold.

Exemple:


```
#!c

int main()
{
    int    a;
    int    b;

    a = (random() % 4) - 1;
    // a can be any value inside the range [-1, 3]
    b = (random() % 4) + 10;
    // b can be any value inside the range [10, 14]
    printf("%d\n", a + b);
    // The result of (a + b) can be any value between [9, 17]
    return (c);
}

```

## Context

ValEmulator is developed as part of the Barghest project (barghest.org).
