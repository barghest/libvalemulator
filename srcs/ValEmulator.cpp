/// Copyright (c) 2016, Jean-Baptiste Laurent
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
///
/// 1. Redistributions of source code must retain the above copyright notice, this
///    list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation
///    and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
/// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
/// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
/// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
/// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
/// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
/// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
/// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
/// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
///
/// The views and conclusions contained in the software and documentation are those
/// of the authors and should not be interpreted as representing official policies,
/// either expressed or implied, of the Barghest Project.
///

#include <iostream>        // operator<<(...)
#include "ValEmulator.hh"

template <typename T>
ValEmulator<T>::ValEmulator()
  : _bitFieldArith(this->_bitFieldMem.data())
{
  this->reset();
}

template <typename T>
ValEmulator<T>::ValEmulator(T const &val)
  : _bitFieldArith(this->_bitFieldMem.data())
{
  this->_bitFieldArith = val;
  this->_eflags.clear();
}

template <typename T>
ValEmulator<T>::~ValEmulator(void)
{
}

template <typename T>
auto    ValEmulator<T>::set(T const &val) -> void
{
  this->_bitFieldArith = val;
}

template <typename T>
auto    ValEmulator<T>::reset(void) -> void
{
  this->_bitFieldArith.setAllToUndefined();
  this->resetEflags();
  // this->_bitFieldArith = static_cast<T>(0);
  // this->clrEflags();
}


// template <typename T>
// auto    ValEmulator<T>::empty(void) const -> bool
// {
// }

// template <typename T>
// auto    ValEmulator<T>::whole(void) const -> bool
// {
// }


template <typename T>
auto    ValEmulator<T>::getMin(void) const -> T
{
  return (this->_bitFieldArith.getMin());
}

template <typename T>
auto    ValEmulator<T>::getMax(void) const -> T
{
  return (this->_bitFieldArith.getMax());
}

template <typename T>
auto    ValEmulator<T>::getEflags(void) const
  ->  bitField::BitFieldArithmetic::t_bitFieldEflags const &
{
  return (this->_eflags);
}

template <typename T>
auto    ValEmulator<T>::clrEflags(void) -> void
{
  this->_eflags.clear();
}

template <typename T>
auto    ValEmulator<T>::resetEflags(void) -> void
{
  this->_eflags.setAllTo(bitField::Undefined);
}

// template <typename T>
// auto    ValEmulator<T>::lower(void) const -> T
// {
// }

// template <typename T>
// auto    ValEmulator<T>::upper(void) const -> T
// {
// }

// template <typename T>
// auto    ValEmulator<T>::width(void) const -> T
// {
// }

// template <typename T>
// auto    ValEmulator<T>::overlap(ValEmulator<T> const &) -> e_inc
// {
// }

// template <typename T>
// auto    ValEmulator<T>::subset(ValEmulator<T> const &) -> e_inc
// {
// }


// template <typename T>
// auto    ValEmulator<T>::addVal(T const &) -> void
// {
// }

// template <typename T>
// auto    ValEmulator<T>::delVal(T const &) -> void
// {
// }


// template <typename T>
// auto    ValEmulator<T>::contain(T const &) const -> bool
// {
// }

// template <typename T>
// auto    ValEmulator<T>::exclude(T const &) const -> bool
// {
// }


// template <typename T>
// auto    ValEmulator<T>::operator=(ValEmulator<T> const &) -> ValEmulator<T> &
// {
// }

// template <typename T>
// auto    ValEmulator<T>::operator==(ValEmulator<T> const &) const -> e_inc
// {
// }


template <typename T>
auto    ValEmulator<T>::operator+=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorAddEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

// template <typename T>
// auto    ValEmulator<T>::operator++(void) -> ValEmulator<T> &
// {
// return (*this);
// }

template <typename T>
auto    ValEmulator<T>::operator-=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorSubEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

// template <typename T>
// auto    ValEmulator<T>::operator--(void) -> ValEmulator<T> &
// {
// return (*this);
// }

template <typename T>
auto    ValEmulator<T>::operator*=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorMultEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

template <typename T>
auto    ValEmulator<T>::operator/=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorDivEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

// template <typename T>
// auto    ValEmulator<T>::operator%=(ValEmulator<T> const &other) -> ValEmulator<T> &
// {
//   return (*this);
// }

template <typename T>
auto    ValEmulator<T>::operator<<=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorLShiftEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

template <typename T>
auto    ValEmulator<T>::operator>>=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorRShiftEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

template <typename T>
auto    ValEmulator<T>::operator|=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorOrEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

template <typename T>
auto    ValEmulator<T>::operator&=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorAndEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}

template <typename T>
auto    ValEmulator<T>::operator^=(ValEmulator<T> const &other) -> ValEmulator<T> &
{
  this->_bitFieldArith.operatorXorEqual(other._bitFieldArith, this->_eflags);
  return (*this);
}


template <typename T>
auto    operator<<(std::ostream &os, ValEmulator<T> const &other) -> std::ostream &
{
  os << "bitField prettyprint: " << std::endl;
  os << other._bitFieldArith;
  return (os);
}

template class ValEmulator <uint8_t>;
template class ValEmulator <uint16_t>;
template class ValEmulator <uint32_t>;

template class ValEmulator <int8_t>;
template class ValEmulator <int16_t>;
template class ValEmulator <int32_t>;

#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
template class ValEmulator <uint64_t>;
template class ValEmulator <int64_t>;
#endif /* !64b */
